SNOWFLAKE_METADATA_DATABASE="DEV_ION"
SNOWFLAKE_METADATA_SCHEMA="WEBHOOK"
AWS_REGION="ap-southeast-2"
AWS_ACCOUNT_ID="$( aws sts get-caller-identity --output text | awk '{print $1}' )"
S3_BUCKET="syncpartner-snowflake-webhook-codexdev"
#S3_REQUEST_PREFIX="request"
S3_DATA_PREFIX="data"
if [[ -z $AWS_ACCOUNT_ID ]]; then
  echo ERROR Failed getting AWS account ID
  exit 1
fi

S3_BUCKET="syncpartner-snowflake-webhook-codexdev"
#S3_REQUEST_PREFIX="request"
S3_DATA_PREFIX="data"

cat setup.sql.template \
  | sed -e "s~{SNOWFLAKE_METADATA_DATABASE}~$SNOWFLAKE_METADATA_DATABASE~g" \
  | sed -e "s~{SNOWFLAKE_METADATA_SCHEMA}~$SNOWFLAKE_METADATA_SCHEMA~g" \
  | sed -e "s~{S3_BUCKET}~$S3_BUCKET~g" \
  | sed -e "s~{S3_DATA_PREFIX}~$S3_DATA_PREFIX~g" \
  | sed -e "s~{AWS_ACCOUNT_ID}~$AWS_ACCOUNT_ID~g" \
  > setup.sql
#cat serverless.yaml.template | sed -e "s~{S3_BUCKET}~$S3_BUCKET~g" > serverless.yaml
#cat lambda.js.template  | sed -e "s~{AWS_S3_REGION}~$AWS_S3_REGION~g" > lambda.js

