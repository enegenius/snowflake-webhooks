use role SYSADMIN;
create database if not exists DEV_ION;
use database DEV_ION;
create schema if not exists DEV_ION.WEBHOOK;
use schema DEV_ION.WEBHOOK;


-- Create a JSON file format
create or replace file format json
  type = json
  compression = none;


use role accountadmin;

-- Create integration
create or replace storage integration S3_WEBHOOK_INT
  type = external_stage
  storage_provider = s3
  enabled = true
  storage_aws_role_arn = 'arn:aws:iam::672106751348:role/ServerlessSyncPartnerSnowflakeWebhookRole-codexdev'
  storage_allowed_locations = ('s3://syncpartner-snowflake-webhook-codexdev/')
  comment = 'AWS S3 storage integration for Snowflake to be able to trigger webhooks via an S3 Lambda trigger';

grant usage on integration S3_WEBHOOK_INT to role sysadmin;

use role sysadmin;

-- Create stage to access the S3 bucket for webhook call notifications
create or replace stage webhook_calls
url='s3://syncpartner-snowflake-webhook-codexdev/'
storage_integration = S3_WEBHOOK_INT
file_format = ( format_name = json );

-- create stage to write webhook data to
create or replace stage webhook_data
url='s3://syncpartner-snowflake-webhook-codexdev/data/'
storage_integration = S3_WEBHOOK_INT
file_format = ( format_name = json );

-- list @webhook_calls;

CREATE OR REPLACE TRANSIENT TABLE DEV_ION."WEBHOOK"."WEBHOOK_LOG" (
                                                    WEBHOOK_CREATE_TS TIMESTAMP_NTZ,
                                                    WEBHOOK_COMPLETE_TS TIMESTAMP_NTZ,
                                                    WEBHOOK_ID varchar(36),
                                                    WEBHOOK_SYNC_ID varchar(36),
                                                    WEBHOOK_TYPE VARCHAR(5),
                                                    WEBHOOK_METHOD VARCHAR(5),
                                                    WEBHOOK_URL VARCHAR,
                                                    WEBHOOK_PARAMS VARCHAR,
                                                    WEBHOOK_STATUS VARCHAR,
                                                    WEBHOOK_RESPONSE VARCHAR,
                                                    WEBHOOK_HTTP_RESPONSE VARCHAR,
                                                    WEBHOOK_RUNTIME FLOAT
                                                  );


use role SECURITYADMIN;
create or replace role WEBHOOK_METADATA_UPDATE_ROLE;
grant UPDATE on table DEV_ION."WEBHOOK"."WEBHOOK_LOG" to role WEBHOOK_METADATA_UPDATE_ROLE;
create or replace user WEBHOOK_METADATA_UPDATE_user password='evwevkjwbvkjwegvwej' default_role = WEBHOOK_METADATA_UPDATE_ROLE;
use role SYSADMIN;




create or replace procedure call_webhook_async("id" varchar, "url" varchar, "method" varchar, "params" varchar)
  returns string not null
  language javascript
  as
  $$
    // Generate a UUID
    var sqlText = `SELECT UUID_STRING()`
    var rs = snowflake.createStatement({ sqlText }).execute()
    rs.next()
    var uuid = rs.getColumnValueAsString(1)


    var return_var = ""

    var my_sql_command =`INSERT INTO DEV_ION."WEBHOOK"."WEBHOOK_LOG" 
            (WEBHOOK_CREATE_TS,WEBHOOK_ID,WEBHOOK_URL,WEBHOOK_METHOD,WEBHOOK_PARAMS,WEBHOOK_STATUS,WEBHOOK_TYPE,WEBHOOK_SYNC_ID) 
            VALUES (current_timestamp()::timestamp_ntz,'${id}','${url}','${method}','${params}','async - no feedback available','async','${uuid}')`;

    var statement1 = snowflake.createStatement( {sqlText: my_sql_command} );
    var result_set1 = statement1.execute();
    return_var=return_var.concat(my_sql_command)

                                                    
                                                    
    // Dump the table out to S3 (using a stage, obvs)
    sqlText = `copy into @webhook_calls/request/${uuid} from (select 
        object_construct('id', WEBHOOK_SYNC_ID, 'url', WEBHOOK_URL, 'method', WEBHOOK_METHOD, 'payload', WEBHOOK_PARAMS) 
        from DEV_ION."WEBHOOK"."WEBHOOK_LOG" where WEBHOOK_SYNC_ID='${uuid}')`
    snowflake.createStatement({ sqlText }).execute()
    return_var=return_var.concat(sqlText)
    //return return_var; // Statement returned for info/debug purposes
    return 'Webhook sucessfully registered for execution'
  $$;



create or replace procedure call_webhook_sync("id" varchar, "url" varchar, "method" varchar, "params" varchar)
  returns string not null
  language javascript
  as
  $$
    // Generate a UUID
    var sqlText = `SELECT UUID_STRING()`
    var rs = snowflake.createStatement({ sqlText }).execute()
    rs.next()
    var uuid = rs.getColumnValueAsString(1)


    var return_var = ""

    var my_sql_command =`INSERT INTO DEV_ION."WEBHOOK"."WEBHOOK_LOG" 
            (WEBHOOK_CREATE_TS,WEBHOOK_ID,WEBHOOK_URL,WEBHOOK_METHOD,WEBHOOK_PARAMS,WEBHOOK_STATUS,WEBHOOK_TYPE,WEBHOOK_SYNC_ID) 
            VALUES (current_timestamp()::timestamp_ntz,'${id}','${url}','${method}','${params}','unprocessed','sync','${uuid}')`;

    var statement1 = snowflake.createStatement( {sqlText: my_sql_command} );
    var result_set1 = statement1.execute();
    return_var=return_var.concat(my_sql_command)

                                                    
                                                    
    // Dump the table out to S3 (using a stage, obvs)
    sqlText = `copy into @webhook_calls/request/${uuid} from (select 
        object_construct('id', WEBHOOK_SYNC_ID, 'url', WEBHOOK_URL, 'method', WEBHOOK_METHOD, 'payload', WEBHOOK_PARAMS) 
        from DEV_ION."WEBHOOK"."WEBHOOK_LOG" where WEBHOOK_SYNC_ID='${uuid}')`
    snowflake.createStatement({ sqlText }).execute()
    return_var=return_var.concat(sqlText)

    
    
  // Poll for the response JSON file to appear in S3
  const sleepMS = 1000
  const timeoutMS = 30000
  let found = false
  let start = new Date().getTime()
  let response = null
  do {

    // Wait
    sqlText = `call system$wait(${sleepMS}, 'MILLISECONDS')`
    
    snowflake.createStatement({ sqlText }).execute()

    // Check for the response in the stage
    sqlText = `select $1 from @webhook_calls/response/${uuid}`
    return_var=return_var.concat(sqlText)
    let rs = snowflake.createStatement({ sqlText }).execute()
    found = rs.next()
    if(found) response = rs.getColumnValueAsString(1)
    return_var=return_var.concat(response)

  } while(!found && new Date().getTime() < start + timeoutMS)
  
  
  // TO DO - Add better handling and reporting to a user when a) a timeout hits in the Lambda function and b) when a timeout hits in the above loop
  

    // Now result file is in the stage, merge the results back into the main webhook table
    sqlText = `merge into DEV_ION."WEBHOOK"."WEBHOOK_LOG" m
  using (
  select  parse_json($1):statusCode as status,parse_json($1):body as body from @webhook_calls/response/${uuid}.json) s 
  on m.WEBHOOK_SYNC_ID = '${uuid}'
  when matched then update set m.WEBHOOK_STATUS = 'processed', m.WEBHOOK_RESPONSE = s.body, m.WEBHOOK_HTTP_RESPONSE = s.status, m.WEBHOOK_COMPLETE_TS=current_timestamp()::timestamp_ntz;`
    return_var=return_var.concat(sqlText)
    

    rs = snowflake.createStatement({ sqlText }).execute()
    found = rs.next()
    
    if(found) response = rs.getColumnValueAsString(1)
    return_var=return_var.concat(response)
    

    // Pull the body response to return with the function return
    var responsebody='Error, response body cannot be found'
    sqlText = `select WEBHOOK_RESPONSE, WEBHOOK_HTTP_RESPONSE from DEV_ION."WEBHOOK"."WEBHOOK_LOG" where WEBHOOK_SYNC_ID = '${uuid}';`
    return_var=return_var.concat(sqlText)

    rs = snowflake.createStatement({ sqlText }).execute()
    found = rs.next()
    
 
    if(found) responsebody = rs.getColumnValueAsString(1)
    if(found) responsestatus = rs.getColumnValueAsString(2)
    let finalresponse='{"httpStatusCode":"520","body":"An unknown error has occurred"}'
    if(responsebody != 'null') finalresponse=`{"httpStatusCode":"${responsestatus}","body":"${responsebody}"}`
    return_var=return_var.concat(finalresponse)

  //return "DONE"
  //return return_var; // Statement returned for info/debug purposes
  return finalresponse;
    
  $$;




show procedures;
